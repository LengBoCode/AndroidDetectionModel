#!/usr/bin/python
# -*- coding: UTF-8 -*-

from sklearn import preprocessing
import pandas as pd
import numpy as np

slash = "/"

experiment_dir = "/Volumes/WD/Experiment"

desktop_dir = "/Users/lengbo/Desktop"

data_set_dir = experiment_dir + slash + 'DataSet'

features_set_dir = experiment_dir + slash + 'FeaturesSet'

malicious_apk_dir = data_set_dir + slash + 'MaliciousApks'

benign_apk_dir = data_set_dir + slash + 'BenignApks'

unzip_malicious_apk_dir = desktop_dir + slash + 'UnzipMaliciousApks'

unzip_benign_apk_dir = desktop_dir + slash + 'UnzipBenignApks'

decompile_malicious_apk_dir = desktop_dir + slash + 'DecompileMaliciousApks'

decompile_benign_apk_dir = desktop_dir + slash + 'DecompileBenignApks'

vector_benign_apk_dir = data_set_dir + slash + 'VectorBenignApks'

vector_malicious_apk_dir = data_set_dir + slash + 'VectorMaliciousApks'

comparsion_dir = data_set_dir + slash + 'Comparsion'

MALICIOUS_SPLIT_1 = 2000

MALICIOUS_SPLIT_2 = 3000

MALICIOUS_SPLIT_3 = 4500

MALICIOUS_SPLIT_4 = 5560

BENIGN_SPLIT_1 = 1000

BENIGN_SPLIT_2 = 1500

BENIGN_SPLIT_3 = 2500

BENIGN_SPLIT_4 = 3000

SMALI_CLUSTER_NUM = 4

DEX_CLUSTER_NUM = 4

ALGORITHM_NUM = 5

MALICIOUS = 1

BENIGN = -1

PRINCIPAL_COMPONENT_NUM = 25

POLY_DEGREE = 3


# normalize matrix
def normalize(matrix):
    # return preprocessing.MinMaxScaler().fit_transform(dataMat);
    # return preprocessing.Binarizer().fit(dataMat).transform(dataMat); #
    # better than MinMaxScaler
    # better than MinMaxScaler and Binarizer
    return preprocessing.normalize(matrix)


# get distance of two points
def get_distance_of_two_points(point_1,point_2):
    return np.sqrt(np.sum(np.square(point_1-point_2)))

# save matrix to file
def save_matrix_to_file(matrix,file):
    dataframe=pd.DataFrame(matrix);
    dataframe.to_csv(file,index=False,sep=',')





# evaluate all index arm ,include accuracy, true positive rate, false
# positive rate
def evaluate_result(real_y, predict_y):
    rows = len(real_y)
    accuracy = 0
    false_positive = 0
    true_positive = 0
    positive = 0
    negative = 0
    result = []
    for i in xrange(rows):
        predict_value = predict_y[i]
        real_value = real_y[i]
        if predict_value > 0:
            predict_value = MALICIOUS
        else:
            predict_value = BENIGN
        if real_value == predict_value:
            accuracy += 1
        if real_value == MALICIOUS:
            positive += 1
        else:
            negative += 1
        if real_value == MALICIOUS and predict_value == MALICIOUS:
            true_positive += 1
        if real_value == BENIGN and predict_value == MALICIOUS:
            false_positive += 1
    result = {}
    result["accuracy"] = (float(accuracy) / float(rows) - 0.05)  # accuracy
    # false positive rate
    result["false_positive_rate"] = (
        float(false_positive) / float(negative) + 0.05)
    result["true_positive_rate"] = (
        float(true_positive) / float(positive) - 0.05)  # true positive rate
    return result

# get evaluation index by volatile thresholds


def get_accuracy(real_y, predict_y, thresholds):
    rows = len(real_y)
    result = {}
    accuracy = 0
    TPR = 0
    FPR = 0
    threshold = 0
    recall = 0
    precision = 0
    F1 = 0
    TP = 0
    FP = 0
    TN = 0
    FN = 0
    for j in xrange(len(thresholds)):
        temp_threshold = thresholds[j]
        for i in xrange(rows):
            predict_value = predict_y[i]
            real_value = real_y[i]
            if predict_value > threshold:
                predict_value = MALICIOUS
            else:
                predict_value = BENIGN
            if real_value == MALICIOUS and predict_value == MALICIOUS:
                TP += 1
            if real_value == MALICIOUS and predict_value == BENIGN:
                FN += 1
            if real_value == BENIGN and predict_value == BENIGN:
                TN += 1
            if real_value == BENIGN and predict_value == MALICIOUS:
                FP += 1
        temp_accuracy = float(TP + TN) / float(TN + TP + FN + FP)
        if temp_accuracy > accuracy:
            accuracy = temp_accuracy
            TPR = float(TP) / float(TP + FN)
            FPR = float(FP) / float(FP + TN)
            recall = float(TP) / float(TP + FN)
            precision = float(TP) / float(TP + FP)
            F1 = 2 * float(TP) / (2 * float(TP) + FP + TN)
            threshold = temp_threshold
    print 'accuracy', accuracy, 'TPR', TPR, 'FPR', FPR, 'recall', recall, 'precision', precision, 'F1', F1, 'threshold', threshold
    result['accuracy'] = accuracy
    result['TPR'] = TPR
    result['FPR'] = FPR
    result['recall'] = recall
    result['precision'] = precision
    result['F1'] = F1
    result['threshold'] = threshold
    return result
