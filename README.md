您好，非常感谢您对“一种基于统计学特征的android恶意应用检测方法”的关注。

# 运行该分类模型，您需要满足下列条件

1，安装python2.7以及sklearn，numpy，matplotlib，gcc等库

# 运行步骤

1，进入PCSD/BuildModel目录下

2，修改PcsdDeal.py文件中138行apk_file="/Volumes/WD/Experiment/DataSet/BenignApks/com.ua.record.apk"成需要处理的apk文件

3，运行python PcsdDeal.py，如果提示权限不足，请使用root用户运行
