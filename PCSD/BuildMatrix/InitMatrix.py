#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
sys.path.append('../../Common/')
from numpy import *
from Common import *



# get smali matrix by ****_smali_info.txt
def get_smali_matrix_by_info_file(file):
    fp = open(file, 'r')
    smali_matrix = []
    for line in fp.readlines():
        lineArr = line.strip().split(' ')
        temp = []
        for value in lineArr:
            # print value
            if value.find('apkname') >= 0:
                continue
            temp.append(float(value.split(':')[1]))
        smali_matrix.append(temp)
    fp.close()
    return mat(smali_matrix)

# get manifest matrix by ****_manifest_info.txt
def get_manifest_matrix_by_info_file(file):
    fp = open(file)
    matrix = []
    count = 1
    for line in fp.readlines():
        lineArr = line.strip().split('/')[0].split(' ')
        temp = []
        if len(lineArr) == 2:
            temp.append(float(0))
        else:
            value = lineArr[len(lineArr) - 2].split(':')[1]
            temp.append(float(value))
        lineArr = line.strip().split('/')[1].split(' ')
        for value in lineArr:
            if value.find('component') >= 0 or value == '':
                continue
            temp.append(float(value.split(':')[1]))
        matrix.append(temp)
    fp.close();
    return mat(matrix)

# get dex matrix by ****_dex_info.txt
def get_dex_matrix_by_info_file(file):
    fp = open(file)
    matrix = []
    for line in fp.readlines():
        lineArr=line.strip().split(" ");
        temp=[]
        for value in lineArr:
            if value.find("apkname")>=0:
                continue
            temp.append(float(value.split(":")[1]));
        matrix.append(temp);
    fp.close();
    return mat(matrix)

# get malicious info matrix combined manifest and smali
def get_malicious_manifest_and_smali_matrix():
    smali_matrix= get_smali_matrix_by_info_file("../Material/malicious_smali_info.txt");
    manifest_matrix= get_manifest_matrix_by_info_file("../Material/malicious_manifest_info.txt");
    return hstack((smali_matrix,manifest_matrix));

# get malicious`s info matrix combined manifest and dex
def get_malicious_manifest_and_dex_matrix():
    dex_matrix= get_smali_matrix_by_info_file("../Material/malicious_dex_info.txt");
    manifest_matrix= get_manifest_matrix_by_info_file("../Material/malicious_manifest_info.txt");
    # print shape(dex_matrix),shape(manifest_matrix);
    return hstack((dex_matrix,manifest_matrix));

# get benign info matrix combined manifest and smali
def get_benign_manifest_and_smali_matrix():
    smali_matrix= get_smali_matrix_by_info_file("../Material/benign_smali_info.txt");
    manifest_matrix= get_manifest_matrix_by_info_file("../Material/benign_manifest_info.txt");
    return hstack((smali_matrix,manifest_matrix));


# get benign info matrix combined manifest and dex
def get_benign_manifest_and_dex_matrix():
    dex_matrix= get_dex_matrix_by_info_file("../Material/benign_dex_info.txt");
    manifest_matrix= get_manifest_matrix_by_info_file("../Material/benign_manifest_info.txt");
    return hstack((dex_matrix,manifest_matrix));
