#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
sys.path.append('../../Common/')
from numpy import *
from sklearn import preprocessing
from Common import *
from InitMatrix import *
import pickle


# add label for matrix and combine malicious and benign
def add_label_and_combine(malicious_matrix, benign_matrix):
    m, n = shape(malicious_matrix)
    k, l = shape(benign_matrix)
    result = zeros((m + k, n + 1))
    result[0:m, 0:-1] = malicious_matrix
    result[m:m + k, 0:-1] = benign_matrix
    result = normalize(result)
    result[0:m, -1] = MALICIOUS
    result[m:m + k, -1] = BENIGN
    return mat(result)


# deal matrix by cluster
def deal_matrix_by_cluster(matrix, kind):
    if kind == "dex":
        fp = open("../Material/dex_cluster_best_algorithm.pkl", "r")
    else:
        fp = open("../Material/smali_cluster_best_algorithm.pkl", "r")
    temps = pickle.load(fp)
    train_matrix = []
    for temp in temps:
        temp_result = temp["clf"].predict(matrix[:, 0:-1])
        length = len(shape(temp_result))
        temp_result_2 = []
        if length == 2:
            for x in xrange(shape(temp_result)[0]):
                temp_result_2.append(temp_result[x][0])
            train_matrix.append(temp_result_2)
            continue
        train_matrix.append(temp["clf"].predict(matrix[:, 0:-1]).tolist())
    train_matrix = mat(train_matrix).T
    return train_matrix

# get first train matrix about smali info by cluster info
# you need to call smali_info_cluster in ../BuildModel/ClusterDeal.py first


def build_smali_train_matrix_by_cluster_info():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    benign_matrix = get_benign_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[0:MALICIOUS_SPLIT_1]
    benign_matrix = benign_matrix[0:BENIGN_SPLIT_1]
    fp = open("../Material/smali_cluster_info.pkl", "rb")
    cluster_info = pickle.load(fp)
    row = 0
    result = {}
    for x in range(SMALI_CLUSTER_NUM):
        temp = []
        result.setdefault(x, temp)
    for x in cluster_info:
        result[x].append(malicious_matrix[row].tolist()[0])
        row = row + 1
    for x in range(SMALI_CLUSTER_NUM):
        result[x] = add_label_and_combine(mat(result[x]), benign_matrix)
    return result


# get first test matrix about smali info
def build_smali_test_matrix_1():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    benign_matrix = get_benign_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_1:MALICIOUS_SPLIT_2]
    benign_matrix = benign_matrix[BENIGN_SPLIT_1:BENIGN_SPLIT_2]
    return add_label_and_combine(malicious_matrix, benign_matrix)


# get second train matrix about smali info
def build_smali_train_matrix_2():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    benign_matrix = get_benign_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_2:MALICIOUS_SPLIT_3]
    benign_matrix = benign_matrix[BENIGN_SPLIT_2:BENIGN_SPLIT_3]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    train_matrix = deal_matrix_by_cluster(result, "smali")
    final_result = zeros((shape(train_matrix)[0], SMALI_CLUSTER_NUM + 1))
    final_result = mat(final_result)
    final_result[:, 0:-1] = train_matrix
    final_result[:, -1] = result[:, -1]
    return final_result


# get second test matrix about smali info
def build_smali_test_matrix_2():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    benign_matrix = get_benign_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_3:MALICIOUS_SPLIT_4]
    benign_matrix = benign_matrix[BENIGN_SPLIT_3:BENIGN_SPLIT_4]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    train_matrix = deal_matrix_by_cluster(result, "smali")
    final_result = zeros((shape(train_matrix)[0], SMALI_CLUSTER_NUM + 1))
    final_result = mat(final_result)
    final_result[:, 0:-1] = train_matrix
    final_result[:, -1] = result[:, -1]
    return final_result


# get first train matrix about dex info by cluster info
# you have to call dex_cluster_info in "../BuildModel/ClusterDeal.py" first
def build_dex_train_matrix_by_cluster_info():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    benign_matrix = get_benign_manifest_and_dex_matrix()
    malicious_matrix = malicious_matrix[0:MALICIOUS_SPLIT_1]
    benign_matrix = benign_matrix[0:BENIGN_SPLIT_1]
    fp = open("../Material/dex_cluster_info.pkl", "rb")
    cluster_info = pickle.load(fp)
    row = 0
    result = {}
    for x in range(SMALI_CLUSTER_NUM):
        temp = []
        result.setdefault(x, temp)
    for x in cluster_info:
        result[x].append(malicious_matrix[row].tolist()[0])
        row = row + 1
    for x in range(SMALI_CLUSTER_NUM):
        result[x] = add_label_and_combine(mat(result[x]), benign_matrix)
    return result


# get first test matrix about dex info
def build_dex_test_matrix_1():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    benign_matrix = get_benign_manifest_and_dex_matrix()
    print shape(malicious_matrix), shape(benign_matrix)
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_1:MALICIOUS_SPLIT_2]
    benign_matrix = benign_matrix[BENIGN_SPLIT_1:BENIGN_SPLIT_2]
    return add_label_and_combine(malicious_matrix, benign_matrix)


# get second train matrix about dex info
def build_dex_train_matrix_2():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    benign_matrix = get_benign_manifest_and_dex_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_2:MALICIOUS_SPLIT_3]
    benign_matrix = benign_matrix[BENIGN_SPLIT_2:BENIGN_SPLIT_3]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    train_matrix = deal_matrix_by_cluster(result, "dex")
    final_result = zeros((shape(train_matrix)[0], DEX_CLUSTER_NUM + 1))
    final_result = mat(final_result)
    final_result[:, 0:-1] = train_matrix
    final_result[:, -1] = result[:, -1]
    return final_result


# get second test matrix about dex info
def build_dex_test_matrix_2():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    benign_matrix = get_benign_manifest_and_dex_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_3:MALICIOUS_SPLIT_4]
    benign_matrix = benign_matrix[BENIGN_SPLIT_3:BENIGN_SPLIT_4]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    train_matrix = deal_matrix_by_cluster(result, "dex")
    final_result = zeros((shape(train_matrix)[0], DEX_CLUSTER_NUM + 1))
    final_result = mat(final_result)
    final_result[:, 0:-1] = train_matrix
    final_result[:, -1] = result[:, -1]
    return final_result


#####################################################################################
#####################################################################################
#####################################################################################
# about clutering or without clustering
#####################################################################################
#####################################################################################
#####################################################################################

# About Smali
# get first train matrix about smali info
def build_entity_smali_train_matrix_1():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    benign_matrix = get_benign_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[0:MALICIOUS_SPLIT_1]
    benign_matrix = benign_matrix[0:BENIGN_SPLIT_1]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    return result


# get first test matrix about smali info
def build_entity_smali_test_matrix_1():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    benign_matrix = get_benign_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_1:MALICIOUS_SPLIT_2]
    benign_matrix = benign_matrix[BENIGN_SPLIT_1:BENIGN_SPLIT_2]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    return result


# get second train matrix about smali info
def build_entity_smali_train_matrix_2():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    benign_matrix = get_benign_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_2:MALICIOUS_SPLIT_3]
    benign_matrix = benign_matrix[BENIGN_SPLIT_2:BENIGN_SPLIT_3]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    return result


# get second test matrix about smali info
def build_entity_smali_test_matrix_2():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    benign_matrix = get_benign_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_3:MALICIOUS_SPLIT_4]
    benign_matrix = benign_matrix[BENIGN_SPLIT_3:BENIGN_SPLIT_4]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    return result


# About Dex
# get first train matrix about dex info
def build_entity_dex_train_matrix_1():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    benign_matrix = get_benign_manifest_and_dex_matrix()
    malicious_matrix = malicious_matrix[0:MALICIOUS_SPLIT_1]
    benign_matrix = benign_matrix[0:BENIGN_SPLIT_1]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    return result


# get first test matrix about dex info
def build_entity_dex_test_matrix_1():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    benign_matrix = get_benign_manifest_and_dex_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_1:MALICIOUS_SPLIT_2]
    benign_matrix = benign_matrix[BENIGN_SPLIT_1:BENIGN_SPLIT_2]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    return result


# get second train matrix about dex info
def build_entity_dex_train_matrix_2():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    benign_matrix = get_benign_manifest_and_dex_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_2:MALICIOUS_SPLIT_3]
    benign_matrix = benign_matrix[BENIGN_SPLIT_2:BENIGN_SPLIT_3]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    return result


# get second test matrix about dex info
def build_entity_dex_test_matrix_2():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    benign_matrix = get_benign_manifest_and_dex_matrix()
    malicious_matrix = malicious_matrix[MALICIOUS_SPLIT_3:MALICIOUS_SPLIT_4]
    benign_matrix = benign_matrix[BENIGN_SPLIT_3:BENIGN_SPLIT_4]
    result = add_label_and_combine(malicious_matrix, benign_matrix)
    return result
