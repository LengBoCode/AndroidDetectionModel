#include <stdio.h>
#include <stdlib.h>
#include "typename.h"

struct header_item
{
    ubyte  magic[8];
    //魔法值。更多详情，请参阅上文中“DEX_FILE_MAGIC”下的讨论。
    uint checksum;
    //文件剩余内容（除 magic 和此字段之外的所有内容）的 adler32 校验和；用于检测文件损坏情况
    ubyte signature[20];
    //文件剩余内容（除 magic、checksum 和此字段之外的所有内容）的 SHA-1 签名（哈希）；用于对文件进行唯一标识
    uint file_size;
    //整个文件（包括标头）的大小，以字节为单位
    uint header_size;
    //标头（整个区段）的大小，以字节为单位。这一项允许至少一定程度的向后/向前兼容性，而不必让格式失效。
    uint endian_tag;
    //字节序标记。更多详情，请参阅上文中“ENDIAN_CONSTANT 和 REVERSE_ENDIAN_CONSTANT”下的讨论。
    uint link_size;
    //链接区段的大小；如果此文件未进行静态链接，则该值为 0
    uint link_off;
    //从文件开头到链接区段的偏移量；如果 link_size == 0，则该值为 0。该偏移量（如果为非零值）应该是到 link_data 区段的偏移量。本文档尚未指定此处所指的数据格式；此标头字段（和之前的字段）会被保留为钩子，以供运行时实现使用。
    uint map_off;
    //从文件开头到映射项的偏移量。该偏移量（必须为非零）应该是到 data 区段的偏移量，而数据应采用下文中“map_list”指定的格式。
    uint string_ids_size;
    //字符串标识符列表中的字符串数量
    uint string_ids_off;
    //从文件开头到字符串标识符列表的偏移量；如果 string_ids_size == 0（不可否认是一种奇怪的极端情况），则该值为 0。该偏移量（如果为非零值）应该是到 string_ids 区段开头的偏移量。
    uint type_ids_size;
    //类型标识符列表中的元素数量，最多为 65535
    uint type_ids_off;
    //从文件开头到类型标识符列表的偏移量；如果 type_ids_size == 0（不可否认是一种奇怪的极端情况），则该值为 0。该偏移量（如果为非零值）应该是到 type_ids 区段开头的偏移量。
    uint proto_ids_size;
    //原型标识符列表中的元素数量，最多为 65535
    uint proto_ids_off;
    //从文件开头到原型标识符列表的偏移量；如果 proto_ids_size == 0（不可否认是一种奇怪的极端情况），则该值为 0。该偏移量（如果为非零值）应该是到 proto_ids 区段开头的偏移量。
    uint field_ids_size;
    //字段标识符列表中的元素数量
    uint field_ids_off;
    //从文件开头到字段标识符列表的偏移量；如果 field_ids_size == 0，则该值为 0。该偏移量（如果为非零值）应该是到 field_ids 区段开头的偏移量。
    uint method_ids_size;
    //方法标识符列表中的元素数量
    uint method_ids_off;
    //从文件开头到方法标识符列表的偏移量；如果 method_ids_size == 0，则该值为 0。该偏移量（如果为非零值）应该是到 method_ids 区段开头的偏移量。
    uint class_defs_size;
    //类定义列表中的元素数量
    uint class_defs_off;
    //从文件开头到类定义列表的偏移量；如果 class_defs_size == 0（不可否认是一种奇怪的极端情况），则该值为 0。该偏移量（如果为非零值）应该是到 class_defs 区段开头的偏移量。
    uint data_size;
    //data 区段的大小（以字节为单位）。该数值必须是 sizeof(uint) 的偶数倍。
    uint data_off;
    //从文件开头到data段的偏移地址
};

struct string_id_item
{
    uint string_data_off;
    //从文件开头到此项的字符串数据的偏移量。该偏移量应该是到 data 区段中某个位置的偏移量，
    //而数据应采用下文中“string_data_item”指定的格式。没有偏移量对齐要求。
};

// struct string_data_item
// {
//  utf16_size  uleb128
//  //此字符串的大小；以 UTF-16 代码单元（在许多系统中为“字符串长度”）为单位。也就是说，这是该字符串的解码长度（编码长度隐含在 0 字节的位置）。
//  ubyte data[];
//  //一系列 MUTF-8 代码单元（又称八位字节），后跟一个值为 0 的字节。请参阅上文中的“MUTF-8（修改后的 UTF-8）编码”，了解有关该数据格式的详情和讨论。
// };

struct type_id_item
{
    uint descriptor_idx;
    //此类描述符字符串的 string_ids 列表中的索引。该字符串必须符合上文定义的 TypeDescriptor 的语法。
};


struct proto_id_item
{
    uint shorty_idx;
    //此原型的简短式描述符字符串的 string_ids 列表中的索引。该字符串必须符合上文定义的 ShortyDescriptor 的语法，而且必须与该项的返回类型和参数相对应。
    uint return_type_idx;
    //此原型的返回类型的 type_ids 列表中的索引。
    uint parameters_off;
    //从文件开头到此原型的参数类型列表的偏移量；如果此原型没有参数，则该值为 0。该偏移量（如果为非零值）应该位于 data 区段中，且其中的数据应采用下文中“"type_list"”指定的格式。此外，不得对列表中的类型 void 进行任何引用。
};


struct field_id_item
{
    ushort class_idx;
    //此字段的定义符的 type_ids 列表中的索引。此项必须是“类”类型，而不能是“数组”或“基元”类型。
    ushort type_idx;
    //此字段的类型的 type_ids 列表中的索引。
    uint name_idx;
    //此字段的名称的 string_ids 列表中的索引。该字符串必须符合上文定义的 MemberName 的语法。
};

struct method_id_item
{
    ushort class_idx;
    //此方法的定义符的 type_ids 列表中的索引。此项必须是“类”或“数组”类型，而不能是“基元”类型。
    ushort proto_idx;
    //此方法的原型的 proto_ids 列表中的索引。
    uint name_idx;
    //此方法名称的 string_ids 列表中的索引。该字符串必须符合上文定义的 MemberName 的语法。
};

struct class_def_item
{
    uint class_idx;
    //此类的 type_ids 列表中的索引。此项必须是“类”类型，而不能是“数组”或“基元”类型。
    uint access_flags;
    //类的访问标记（public、final 等）。有关详情，请参阅“access_flags 定义”。
    uint superclass_idx;
    //超类的 type_ids 列表中的索引。如果此类没有超类（即它是根类，例如 Object），则该值为常量值 NO_INDEX。如果此类存在超类，则此项必须是“类”类型，而不能是“数组”或“基元”类型。
    uint interfaces_off;
    //从文件开头到接口列表的偏移量；如果没有接口，则该值为 0。该偏移量应该位于 data 区段，且其中的数据应采用下文中“type_list”指定的格式。该列表的每个元素都必须是“类”类型（而不能是“数组”或“基元”类型），并且不得包含任何重复项。
    uint source_file_idx;
    //文件（包含这个类（至少大部分）的原始来源）名称的 string_ids 列表中的索引；或者该值为特殊值 NO_INDEX，以表示缺少这种信息。任何指定方法的 debug_info_item 都可以覆盖此源文件，但预期情况是大多数类只能来自一个源文件。
    uint annotations_off;
    //从文件开头到此类的注释结构的偏移量；如果此类没有注释，则该值为 0。该偏移量（如果为非零值）应该位于 data 区段，且其中的数据应采用下文中“annotations_directory_item”指定的格式，同时所有项将此类作为定义符进行引用。
    uint class_data_off;
    //从文件开头到此项的关联类数据的偏移量；如果此类没有类数据，则该值为 0（这种情况有可能出现，例如，如果此类是标记接口）。该偏移量（如果为非零值）应该位于 data 区段，且其中的数据应采用下文中“class_data_item”指定的格式，同时所有项将此类作为定义符进行引用。
    uint static_values_off;
    //从文件开头到 static 字段的初始值列表的偏移量；如果没有该列表（并且所有 static 字段都将使用 0 或 null 进行初始化），则该值为 0。该偏移量应该位于 data 区段，且其中的数据应采用下文中“encoded_array_item”指定的格式。该数组的大小不得超出此类所声明的 static 字段的数量，且 static 字段所对应的元素应采用相对应的 field_list 中所声明的顺序每个数组元素的类型均必须与其相应字段的声明类型相匹配。如果该数组中的元素比 static 字段中的少，则剩余字段将使用适当类型 0 或 null 进行初始化。
};


typedef struct
{
    ushort type;
    //项的类型；见下表
    ushort unused;
    //（未使用）
    uint size;
    //在指定偏移量处找到的项数量
    uint offset;
    //从文件开头到相关项的偏移量
} map_item;


uint little_endian_to_big_endian(uint value)
{
    char *c = (char *)&value;
    // return (int) * c + (int)(*(c + 1) << 8) + (int)(*(c + 2) << 16) + (int)(*(c + 3) << 24);
    return (uint)(*c << 24) + (uint)(*(c + 1) << 16) + (uint)(*(c + 2) << 8) + (uint)(*(c + 3));
}


uint uleb128_to_normal(FILE *fp)
{
    byte b[5];
    fread(b, sizeof(byte), 5, fp);
    uint result = 0;
    long count = 0;
    for (int i = 0; i < 5; ++i)
    {
        result = ((result & 0x7f) << (7)) + (b[i] & 0x7f);
        count++;
        if (b[i] <= 0x7f)
        {
            break;
        }
    }
    fseek(fp, count - 5, SEEK_CUR);
    return result;
}

void print_header_item(struct header_item *h_item)
{
    printf("link_size:%d ", h_item->link_size );
    printf("string_ids_size:%d ", h_item->string_ids_size );
    printf("type_ids_size:%d ", h_item->type_ids_size );
    printf("proto_ids_size:%d ", h_item->proto_ids_size );
    printf("field_ids_size:%d ", h_item->field_ids_size );
    printf("method_ids_size:%d", h_item->method_ids_size );
}

int main(int argc, char *args[])
{
    // for(int i = 1; i < argc; i++) //从1开始，跳过第一个命令
    // {
    //     printf("%s\n", args[i]);
    // }
    FILE *fp;
    char *s = args[1];
    if ((fp = fopen(s, "rb")) == NULL)
    {
        printf("file not exist\n");
        return 0;
    }
    struct header_item *h_item = (struct header_item *) malloc(sizeof(struct header_item));
    fread(h_item, sizeof(struct header_item), 1, fp);
    print_header_item(h_item);

    // printf("%s\n", h_item->magic);
    // printf("%d\n", h_item->file_size);
    // printf("%d\n", h_item->method_ids_size);
    // printf("%d\n", h_item->header_size);

    // printf("string_ids_size %d\n", h_item->string_ids_size );
    // struct string_id_item *s_id_item = (struct string_id_item *)malloc(sizeof(struct string_id_item) * h_item->string_ids_size);
    // fread(s_id_item, sizeof(struct string_id_item), h_item->string_ids_size, fp);
    // for (int i = 0; i < h_item->string_ids_size; ++i)
    // {
    //     fseek(fp, (s_id_item + i)->string_data_off, SEEK_SET);
    //     uint s_size = uleb128_to_normal(fp);
    //     printf("string_data_off %d s_size %d\n", (s_id_item + i)->string_data_off, s_size );
    //     char *c = (char *)malloc(sizeof(char) * s_size);
    //     fread(c, sizeof(char), s_size, fp);
    //     printf("result %s\n", c);
    // }




    // printf("type_ids_size %d\n", h_item->type_ids_size );
    // fseek(fp, h_item->type_ids_off, SEEK_SET);
    // struct type_id_item *t_id_item = (struct type_id_item *)malloc(sizeof(struct type_id_item) * h_item->type_ids_size);
    // fread(t_id_item, sizeof(struct type_id_item), h_item->type_ids_size, fp);
    // printf("%d\n", t_id_item->descriptor_idx );


    // printf("proto_ids_size %d\n", h_item->proto_ids_size );
    // struct proto_id_item *p_id_item = (struct proto_id_item *)malloc(sizeof(struct proto_id_item) * h_item->proto_ids_size);
    // fread(p_id_item, sizeof(struct proto_id_item), h_item->proto_ids_size, fp);
    // printf("%d\n", p_id_item->shorty_idx );


    // printf("field_ids_size %d\n", h_item->field_ids_size );
    // struct field_id_item *f_id_item = (struct field_id_item *)malloc(sizeof(struct field_id_item) * h_item->field_ids_size);
    // fread(f_id_item, sizeof(struct field_id_item), h_item->field_ids_size, fp);
    // printf("%d\n", f_id_item->class_idx );


    // printf("method_ids_size %d\n", h_item->method_ids_size );
    // struct method_id_item *m_id_item = (struct method_id_item *)malloc(sizeof(struct method_id_item) * h_item->method_ids_size);
    // fread(m_id_item, sizeof(struct method_id_item), h_item->method_ids_size, fp);
    // printf("%d\n", m_id_item->class_idx );


    // printf("class_defs_size %d\n", h_item->class_defs_size );
    // struct class_def_item *c_def_item = (struct class_def_item *)malloc(sizeof(struct class_def_item) * h_item->class_defs_size);
    // fread(c_def_item, sizeof(struct class_def_item), h_item->class_defs_size, fp);
    // printf("%d\n", c_def_item->class_idx );

    // fseek(fp, h_item->map_off, SEEK_SET);
    // uint *map_size = (uint *)malloc(sizeof(uint));
    // fread(map_size, sizeof(uint), 1, fp);
    // printf("map_size %d\n", *map_size );

    // map_item *m_item = (map_item *)malloc(sizeof(map_item) * (*map_size));
    // fread(m_item, sizeof(map_item), *map_size, fp);
    // printf("%d\n", m_item->type);


    fclose(fp);
    return 0;
}