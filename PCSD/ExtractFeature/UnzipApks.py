#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
sys.path.append('../../Common/') 
import os
import shutil
from Common import *

def get_dex_info(apk_file):
    source=apk_file
    dest="../"+apk_file.split("/")[-1];
    print dest
    print 'unzipping apk from ' + source + '  to ' + dest
    if os.path.exists(dest) == False:
        os.makedirs(dest)
    os.system("unzip -o -j %s \"*.dex\" -d %s" % (source, dest))
    dex_files = []
    for root, dirs, files in os.walk(dest):
        for file in files:
            dex_file = root + '/' + file
            if dex_file.find('.dex') >= 0:
                dex_files.append(dex_file)
    os.system("gcc ../Dex/analysisdex.c -o cmdb");
    dex_info=[];
    link_size=0
    string_ids_size=0
    type_ids_size=0
    proto_ids_size=0
    field_ids_size=0
    method_ids_size=0
    for dex_file in dex_files:
        output=os.popen('./cmdb %s' % dex_file);
        temp_result=output.read();
        temp_arr=temp_result.split(" ");
        link_size=link_size+int(temp_arr[0].split(":")[1]);
        string_ids_size=string_ids_size+int(temp_arr[1].split(":")[1]);
        type_ids_size=type_ids_size+int(temp_arr[2].split(":")[1]);
        proto_ids_size=proto_ids_size+int(temp_arr[3].split(":")[1]);
        field_ids_size=field_ids_size+int(temp_arr[4].split(":")[1]);
        method_ids_size=method_ids_size+int(temp_arr[5].split(":")[1]);
        dex_info.append(temp_result);
    count=len(dex_files);
    link_size=link_size/count;
    string_ids_size=string_ids_size/count;
    type_ids_size=type_ids_size/count;
    proto_ids_size=proto_ids_size/count;
    field_ids_size=field_ids_size/count;
    method_ids_size=method_ids_size/count;
    apkname=source.split("/")[-1];
    result=[];
    result.append(link_size)
    result.append(string_ids_size)
    result.append(type_ids_size)
    result.append(proto_ids_size)
    result.append(field_ids_size)
    result.append(method_ids_size)
    os.system("rm -r -f %s" % 'cmdb');
    os.system("rm -r -f %s" % (dest));
    return result;

def unzip_apk(source, dest,kind):
    print 'unzipping apk from ' + source + '  to ' + dest
    if os.path.exists(dest) == False:
        os.makedirs(dest)
    os.system("unzip -o -j %s \"*.dex\" -d %s" % (source, dest))
    dex_files = []
    for root, dirs, files in os.walk(dest):
        for file in files:
            dex_file = root + '/' + file
            if dex_file.find('.dex') >= 0:
                dex_files.append(dex_file)
    os.system("gcc ../Dex/analysisdex.c -o cmdb");
    dex_info=[];
    link_size=0
    string_ids_size=0
    type_ids_size=0
    proto_ids_size=0
    field_ids_size=0
    method_ids_size=0

    for dex_file in dex_files:
        output=os.popen('./cmdb %s' % dex_file);
        temp_result=output.read();
        temp_arr=temp_result.split(" ");
        link_size=link_size+int(temp_arr[0].split(":")[1]);
        string_ids_size=string_ids_size+int(temp_arr[1].split(":")[1]);
        type_ids_size=type_ids_size+int(temp_arr[2].split(":")[1]);
        proto_ids_size=proto_ids_size+int(temp_arr[3].split(":")[1]);
        field_ids_size=field_ids_size+int(temp_arr[4].split(":")[1]);
        method_ids_size=method_ids_size+int(temp_arr[5].split(":")[1]);
        dex_info.append(temp_result);
    count=len(dex_files);
    link_size=link_size/count;
    string_ids_size=string_ids_size/count;
    type_ids_size=type_ids_size/count;
    proto_ids_size=proto_ids_size/count;
    field_ids_size=field_ids_size/count;
    method_ids_size=method_ids_size/count;
    apkname=source.split("/")[-1];
    result="link_size:"+str(link_size)+" string_ids_size:"+str(string_ids_size)+" type_ids_size:"+str(type_ids_size)+" proto_ids_size:"+str(proto_ids_size)+" field_ids_size:"+str(field_ids_size)+" method_ids_size:"+str(method_ids_size)+" apkname:"+apkname;
    print result;
    if kind=='malicious':
        dex_fp=open('../Material/malicious_dex_info.txt','a')
    else:
        dex_fp=open('../Material/benign_dex_info.txt','a')
    dex_fp.write(result+"\n");
    os.system("rm -r -f %s" % 'cmdb');
    os.system("rm -r -f %s" % (dest));


def unzip_apk_in_dir(sourceDir, destDir,kind):
    apknames = []
    print sourceDir,destDir
    for file in os.listdir(sourceDir.decode('utf-8')):
        if file == '.DS_Store':
            continue
        if file.find('.apk') >= 0:
            apknames.append(file)
    for apkname in apknames:
        apkdir = apkname[0:apkname.rfind('.')]
        source = sourceDir + '/' + apkname
        dest = destDir + '/' + apkdir
        unzip_apk(source, dest,kind)


def unzip_malicious_apk():
    unzip_apk_in_dir(malicious_apk_dir, unzip_malicious_apk_dir,'malicious')


def unzip_benign_apk():
    unzip_apk_in_dir(benign_apk_dir, unzip_benign_apk_dir,'benign')




