#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
sys.path.append('../../Common/') 
sys.path.append('../PCSD/') 
import os
import shutil
from Common import *
from DealSmali import *
from DealManifest import *

def get_mainfest_info(apk_file):
    source=apk_file
    dest=desktop_dir+"/"+apk_file.split("/")[-1];
    print 'decompiling apk from ' + source + '  to ' + dest
    if os.path.exists(dest) == False:
        os.makedirs(dest)
    os.system("apktool -f -o %s d %s" % (dest, source))
    manifest=dest+"/AndroidManifest.xml";
    temp=[]
    if os.path.exists(manifest):
        manifest_info=get_manifest_info_by_apkfile(manifest);
        permission = manifest_info.get('permission')
        component = manifest_info.get('component')
        filename = manifest_info.get('filename')
        permission = sorted(permission.items(), key=lambda d: d[0])
        component = sorted(component.items(), key=lambda d: d[0])
        line = 'permission ' + change_list_to_str(
            permission) + '/' + 'component ' + change_list_to_str(component) + '/' + 'filename ' + filename
        lineArr = line.strip().split('/')[0].split(' ')
        if len(lineArr) == 2:
            temp.append(float(0))
        else:
            value = lineArr[len(lineArr) - 2].split(':')[1]
            temp.append(float(value))
        lineArr = line.strip().split('/')[1].split(' ')
        for value in lineArr:
            if value.find('component') >= 0 or value == '':
                continue
            temp.append(float(value.split(':')[1]))
    else:
        print manifest+"is not exists";

    print "deleting template folder"
    os.system("rm -r -f %s" % (dest));
    return temp;


def get_smali_info(apk_file):
    source=apk_file
    dest="../"+apk_file.split("/")[-1];
    print 'decompiling apk from ' + source + '  to ' + dest
    if os.path.exists(dest) == False:
        os.makedirs(dest)
    os.system("apktool -f -o %s d %s" % (dest, source))
    manifest=dest+"/AndroidManifest.xml";
    temp=[]
    if os.path.exists(manifest):
        smali_info=analysis_certain_apk_smali_by_apkpath(dest);
        result_smali = ''
        for key in smali_info:
            result_smali = result_smali + str(key) + ":" + str(smali_info[key]) + ' '
        print result_smali
        lineArr = result_smali.strip().split(' ')
        for value in lineArr:
            # print value
            if value.find('apkname') >= 0:
                continue
            temp.append(float(value.split(':')[1]))
    else:
        print manifest+"is not exists";

    print "deleting template folder"
    os.system("rm -r -f %s" % (dest));
    return temp;



def decompile_apk(source, dest, kind):
    print 'decompiling apk from ' + source + '  to ' + dest
    if os.path.exists(dest) == False:
        os.makedirs(dest)
    os.system("apktool -f -o %s d %s" % (dest, source))
    manifest=dest+"/AndroidManifest.xml";
    print manifest;
    if os.path.exists(manifest):
        manifest_info=get_manifest_info_by_apkfile(manifest);
        smali_info=analysis_certain_apk_smali_by_apkpath(dest);
        if kind=='malicious':
            manifest_fp=open('../Material/malicious_manifest_info.txt','a')
            smali_fp=open('../Material/malicious_smali_info.txt','a')
        else:
            manifest_fp=open('../Material/benign_manifest_info.txt','a')
            smali_fp=open('../Material/benign_smali_info.txt','a')
        permission = manifest_info.get('permission')
        component = manifest_info.get('component')
        filename = manifest_info.get('filename')
        permission = sorted(permission.items(), key=lambda d: d[0])
        component = sorted(component.items(), key=lambda d: d[0])
        result_manifest = 'permission ' + change_list_to_str(
            permission) + '/' + 'component ' + change_list_to_str(component) + '/' + 'filename ' + filename
        print result_manifest
        result_smali = ''
        for key in smali_info:
            result_smali = result_smali + str(key) + ":" + str(smali_info[key]) + ' '
        print result_smali
        manifest_fp.write(result_manifest+"\n");
        smali_fp.write(result_smali+"\n");
        manifest_fp.close();
        smali_fp.close();
    else:
        print manifest+"is not exists";

    print "deleting template folder"
    os.system("rm -r -f %s" % (dest));


def decompile_apk_in_dir(source_dir, dest_dir, kind):
    apknames = []
    print source_dir,dest_dir
    for file in os.listdir(source_dir.decode('utf-8')):
        if file == '.DS_Store':
            continue
        if file.find('.apk') >= 0:
            apknames.append(file)
    for apkname in apknames:
        apkdir = apkname[0:apkname.rfind('.')]
        source = source_dir + '/' + apkname
        dest = dest_dir + '/' + apkdir
        decompile_apk(source, dest, kind);


def decompile_malicious_apk():
    decompile_apk_in_dir(malicious_apk_dir, decompile_malicious_apk_dir,'malicious');


def decompile_benign_apk():
    decompile_apk_in_dir(benign_apk_dir, decompile_benign_apk_dir,'benign');


