#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
import cPickle as pickle
from Common import *


# befor entering this method,you should check if file exist
def get_component_info_by_file(file):
    fp = open(file)
    component = {}
    filename = file[0:file.rfind('/')]
    filename = filename[filename.rfind('/') + 1:len(filename)]
    component.setdefault('filename', filename)
    component.setdefault('activity', 0)
    component.setdefault('service', 0)
    component.setdefault('receiver', 0)
    component.setdefault('intent-filter', 0)
    component.setdefault('action', 0)
    component.setdefault('category', 0)
    component.setdefault('meta-data', 0)
    for line in fp.readlines():
        if line.find('activity') >= 0:
            component['activity'] += 1
        if line.find('service') >= 0:
            component['service'] += 1
        if line.find('receiver') >= 0:
            component['receiver'] += 1
        if line.find('intent-filter') >= 0:
            component['intent-filter'] += + 1
        if line.find('action') >= 0:
            component['action'] += 1
        if line.find('category') >= 0:
            component['category'] += 1
        if line.find('meta-data') >= 0:
            component['meta-data'] += 1
    fp.close()
    return component


# before using this method ,you should check if file exist
def get_permission_info_by_file(file):
    fp = open(file)
    permission = {}
    filename = file[0:file.rfind('/')]
    filename = filename[filename.rfind('/') + 1:len(filename)]
    permission.setdefault('filename', filename)
    count = 1
    for line in fp.readlines():
        if line.find('uses-permission') >= 0:
            content = line.split(' ')
            for element in content:
                if element.find('android:name="') >= 0:
                    temp_permission = ""
                    if element.find('"/>') >= 0:
                        temp_permission = element[element.find(
                            'android:name="') + len('android:name="'):element.find('"/>')]
                    else:
                        temp_permission = element[element.find(
                            'android:name="') + len('android:name="'):element.rfind('"')]
                    permission.setdefault(count, temp_permission)
                    count = count + 1
    permission.setdefault('count', count - 1)
    return permission


# before using this method ,please check if file exists
def get_manifest_info_by_apkfile(file):
    # print 'dealing with ' + file
    temp_info = {}
    filename = file[0:file.rfind('/')]
    filename = filename[filename.rfind('/') + 1:len(filename)]
    temp_info.setdefault('filename', filename)
    component = get_component_info_by_file(file)
    permission = get_permission_info_by_file(file)
    temp_info.setdefault('component', component)
    temp_info.setdefault('permission', permission)
    return temp_info


def get_manifest_info_by_dir(allapkdir):
    manifest_info = []
    for dirname in os.listdir(allapkdir):
        if os.path.isdir(allapkdir + "/" + dirname):
            file = allapkdir + "/" + dirname + "/AndroidManifest.xml"
            if os.path.exists(file) == False:
                print '*******************************************'
                print file, " is not exist"
                print '*******************************************'
                continue
            temp_info = get_manifest_info_by_apkfile(file)
            manifest_info.append(temp_info)
    return manifest_info


def change_list_to_str(content):
    result = ''
    print content
    for key in content:
        if key[0] == 'filename':
            continue
        result += str(key[0]) + ':' + str(key[1]) + ' '
    return result


def save_manifest_info_to_file(filename, data):
    fp = open(filename, 'wb')
    for line in data:
        permission = line.get('permission')
        component = line.get('component')
        filename = line.get('filename')
        permission = sorted(permission.items(), key=lambda d: d[0])
        component = sorted(component.items(), key=lambda d: d[0])
        result = 'permission ' + change_list_to_str(
            permission) + '/' + 'component ' + change_list_to_str(component) + '/' + 'filename ' + filename
        # print result
        fp.write(result)
        fp.write('\n')
    fp.close()

# this method maybe is wrong
def get_manifest_info_by_file(file):
    fp = open(file)
    # print file
    matrix = []
    count = 1
    for line in fp.readlines():
        print line;
        lineArr = line.strip().split('/')[0].split(' ')
        temp = []
        print lineArr
        if len(lineArr) == 2:
            temp.append(float(0))
        else:
            value = lineArr[len(lineArr) - 2].split(':')[1]
            # print value;
            temp.append(float(value))
        lineArr = line.strip().split('/')[1].split(' ')
        for value in lineArr:
            if value.find('component') >= 0 or value == '':
                continue
            temp.append(float(value.split(':')[1]))
        matrix.append(temp)
    return mat(matrix)


def get_manifest_info_by_cluster_and_filename(cluster, filename):
    fp = open(filename)
    manifest_matrix = []
    container = []
    for line in fp.readlines():
        apkname = line.strip().split('/')[2].split(' ')[1]
        if cluster.count(apkname) == 1:
            container.append(line)
    fp.close()
    for line in container:
        print line;
        lineArr = line.strip().split('/')[0].split(' ')
        temp = []
        if len(lineArr) == 2:
            temp.append(float(0))
        else:
            value = lineArr[len(lineArr) - 2].split(':')[1]
            # print value;
            temp.append(float(value))
        lineArr = line.strip().split('/')[1].split(' ')
        for value in lineArr:
            if value.find('component') >= 0 or value == '':
                continue
            temp.append(float(value.split(':')[1]))
        manifest_matrix.append(temp)
    return mat(manifest_matrix)

def get_test_malicious_manifest_info():
    fp=open('cluster_info.txt')
    apknames=[]
    smali_matrix = []
    for line in fp.readlines():
        apknames.append(line.strip().split(' ')[0])
    fp.close()
    fp=open('malicious_manifest_info.txt')
    manifest_matrix = []
    count = 1
    for line in fp.readlines():
        apkname= line.strip().split('/')[2].split(' ')[1]
        if apknames.count(apkname)==1:
            continue
        lineArr = line.strip().split('/')[0].split(' ')
        temp = []
        # print lineArr
        if len(lineArr) == 2:
            temp.append(float(0))
        else:
            value = lineArr[len(lineArr) - 2].split(':')[1]
            # print value;
            temp.append(float(value))
        lineArr = line.strip().split('/')[1].split(' ')
        for value in lineArr:
            if value.find('component') >= 0 or value == '':
                continue
            temp.append(float(value.split(':')[1]))
        manifest_matrix.append(temp)
    fp.close()
    return mat(manifest_matrix)

