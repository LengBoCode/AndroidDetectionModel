#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
from Common import *

max_parameter_num = 0.0

min_parameter_num = 1000.0

max_register_num = 0.0

min_register_num = 1000.0

max_invoke_num = 0.0

min_invoke_num = 1000.0


def analysis_smali_field_by_file(file):  # get all field kind
    fp = open(file)
    field_info = {}
    field_info.setdefault('field_num', 0.0)
    field_info.setdefault('static_field_num', 0.0)
    field_info.setdefault('private_field_num', 0.0)
    field_info.setdefault('public_field_num', 0.0)
    field_info.setdefault('protected_field_num', 0.0)
    field_info.setdefault('final_field_num', 0.0)
    for line in fp.readlines():
        if line.find('.field') >= 0:  # including .field belongs to field
            field_info['field_num'] += 1
            if line.find('static') >= 0:
                field_info['static_field_num'] += 1
            if line.find('final') >= 0:
                field_info['final_field_num'] += 1
            if line.find('private') >= 0:
                field_info['private_field_num'] += 1
            if line.find('public') >= 0:
                field_info['public_field_num'] += 1
            if line.find('protected') >= 0:
                field_info['protected_field_num'] += 1
    fp.close()
    return field_info


def analysis_smali_method_by_file(file):  # get all method feature
    fp = open(file)
    method_info = {}
    method_info.setdefault('method_num', 0.0)
    method_info.setdefault('static_method_num', 0.0)
    method_info.setdefault('public_method_num', 0.0)
    method_info.setdefault('private_method_num', 0.0)
    method_info.setdefault('protected_method_num', 0.0)
    method_info.setdefault('no_return_method_num', 0.0)
    method_info.setdefault('max_parameter_num', max_parameter_num)
    method_info.setdefault('min_parameter_num', min_parameter_num)
    method_info.setdefault('avg_parameter_num', 0.0)
    method_info.setdefault('max_register_num', max_register_num)
    method_info.setdefault('min_register_num', min_parameter_num)
    method_info.setdefault('avg_register_num', 0.0)
    method_info.setdefault('max_invoke_num', max_invoke_num)
    method_info.setdefault('min_invoke_num', min_invoke_num)
    method_info.setdefault('avg_invoke_num', 0.0)
    total_parameter = 0.0
    total_register = 0.0
    total_invoke = 0.0
    for line in fp.readlines():
        if line.find('.method') >= 0:  # method must include .method
            if len(line.split(')')) == 1:  # but including .method is not always method
                continue
            method_info['method_num'] +=1
            if line.find('static') >= 0:
                method_info['static_method_num'] += 1
            if line.find('public') >= 0:
                method_info['public_method_num'] += 1
            if line.find('private') >= 0:
                method_info['private_method_num'] += 1
            if line.find('protected') >= 0:
                method_info['protected_method_num'] += 1
            if line.strip().split(')')[1] == 'V':
                method_info['no_return_method_num'] += 1
            parameter = len(line.split(';')) - 1

            # print parameter;
            total_parameter = total_parameter + parameter
            if parameter > method_info['max_parameter_num']:
                method_info['max_parameter_num'] = parameter
            if parameter < method_info['min_parameter_num']:
                # print parameter;
                method_info['min_parameter_num'] = parameter
        # represent register number ,which be used
        # if line.find('.locals')>=0:
        #     print line
        if line.find('.locals') >= 0 and line.split('.') >= 2 and line.find(',') < 0 and line[line.find('.locals')+len('.locals'):line.find('.locals')+len('.locals')+1]==' ':
            register = line[line.find('.locals ') + len('.locals '):len(line)]
            register = int(register)
            if register > method_info['max_register_num']:
                method_info['max_register_num'] = register
            if register < method_info['min_register_num']:
                method_info['min_register_num'] = register
            total_register = total_register + register
    if method_info['min_register_num'] == min_register_num:
        method_info['min_register_num'] = 0
        total_register = 0
    fp.close()
    method_invoke = analysis_smali_method_invoke_by_file(file)
    for key in method_invoke:
        total_invoke = total_invoke + method_invoke[key]
        if method_invoke[key] > method_info['max_invoke_num']:
            method_info['max_invoke_num'] = method_invoke[key]
        if method_invoke[key] < method_info['min_invoke_num']:
            method_info['min_invoke_num'] = method_invoke[key]
    if method_info['min_invoke_num'] == min_invoke_num:
        method_info["min_invoke_num"] = 0
    if method_info["min_parameter_num"] == min_parameter_num:
        method_info["min_parameter_num"] = 0
    if method_info['method_num'] != 0:
        method_info['avg_parameter_num'] = float(total_parameter ) / \
            float(method_info['method_num'])
        method_info['avg_register_num'] = float(total_register) / \
            float(method_info['method_num'])
        method_info['avg_invoke_num'] = float(total_invoke) / \
            float(method_info['method_num'])
    return method_info


def get_parameter_num_by_parameter_list(content):
    print content
    parameterArr = content.split('[')
    parameter_num = 0
    array_num = 0
    for parameter in parameterArr:
        array_num += 1
        for value in parameter.split(";"):
            print value
    print parameter_num

# get method invoke inside method


def analysis_smali_method_invoke_by_file(file):
    fp = open(file)
    temp_invoke = []
    for line in fp.readlines():
        if line.find('.method') >= 0:
            temp_invoke.append(line)
        if line.find('invoke-virtual') >= 0:
            temp_invoke.append(line)
        if line.find('invoke-direct') >= 0:
            temp_invoke.append(line)
    # print temp_invoke;
    temp = ''
    result = {}
    for i in range(0, len(temp_invoke)):
        if temp_invoke[i].find('.method') >= 0:
            temp = temp_invoke[i][temp_invoke[i].find(
                ' ') + 1:temp_invoke[i].find('\n')]
            result.setdefault(temp, '')
        else:
            line = temp_invoke[i][temp_invoke[i].find(
                ',') + 2:temp_invoke[i].find('\n')]
            result[temp] = result[temp] + line + ','
    method_invoke = {}  # get method invoke number
    for key in result:
        temp = result[key].split(',')
        if temp > 0:
            method_invoke.setdefault(key, len(temp) - 1)
        else:
            method_invoke.setdefault(key, temp)
    fp.close()
    return method_invoke


def analysis_smali_by_file(file):  # analysis all file
    # print file
    smali_info = {}
    field_info = analysis_smali_field_by_file(file)
    method_info = analysis_smali_method_by_file(file)
    # print field_info, method_info
    for key in field_info:
        smali_info.setdefault(key, field_info[key])
    for key in method_info:
        smali_info.setdefault(key, method_info[key])
    return smali_info


def save_smali_info_to_file(filename, data):
    fp = open(filename, 'w')
    for i in range(len(data)):
        result = data[i]
        line = ''
        for key in result:
            line = line + str(key) + ":" + str(result[key]) + ' '
        fp.write(line)
        fp.write('\n')
    fp.close


def get_all_smali_files_by_dir(smalidir):
    smali_files = []
    for root, dirs, files in os.walk(smalidir):
        for file in files:
            smali_file = root + '/' + file

            if smali_file.find('.smali') >= 0:
                smali_files.append(smali_file)
    return smali_files


def analysis_certain_apk_smali_by_apkpath(apkpath):
    smali_files = get_all_smali_files_by_dir(apkpath)
    smali_info = {}
    smali_info.setdefault('field_num', 0.0)
    smali_info.setdefault('static_field_num', 0.0)
    smali_info.setdefault('private_field_num', 0.0)
    smali_info.setdefault('public_field_num', 0.0)
    smali_info.setdefault('protected_field_num', 0.0)
    smali_info.setdefault('final_field_num', 0.0)
    smali_info.setdefault('method_num', 0.0)
    smali_info.setdefault('static_method_num', 0.0)
    smali_info.setdefault('public_method_num', 0.0)
    smali_info.setdefault('private_method_num', 0.0)
    smali_info.setdefault('protected_method_num', 0.0)
    smali_info.setdefault('no_return_method_num', 0.0)
    smali_info.setdefault('max_parameter_num', 0.0)
    smali_info.setdefault('min_parameter_num', 0.0)
    smali_info.setdefault('avg_parameter_num', 0.0)
    smali_info.setdefault('max_register_num', 0.0)
    smali_info.setdefault('min_register_num', 0.0)
    smali_info.setdefault('avg_register_num', 0.0)
    smali_info.setdefault('max_invoke_num', 0.0)
    smali_info.setdefault('min_invoke_num', 0.0)
    smali_info.setdefault('avg_invoke_num', 0.0)
    count=0
    for file in smali_files:
        # if os.path.exists(file)==False:
        #     count=count+1;
        #     print count,"-----------------------"
        #     print file;
        #     if count==1:
        #         apkname=apkpath.split("/")[-1];
        #         print apkname+" cannot be decompiled"
        #         fp=open('../Material/not_be_decompiled_info.txt','a');
        #         fp.write(apkname+"\n");
        #         fp.close();
        #         continue;
            # continue;
        temp_smali_info = analysis_smali_by_file(file)
        for key in temp_smali_info:
            smali_info[key] = smali_info[key] + temp_smali_info[key]
    length = len(smali_info)
    for key in smali_info:
        smali_info[key] = smali_info[key] / length
    apkname = apkpath[apkpath.rfind('/') + 1:len(apkpath)]
    smali_info.setdefault('apkname', apkname)
    return smali_info


def analysis_smali_info_by_dir(apksdir):
    smali_info = []
    for filename in os.listdir(apksdir):
        if os.path.isdir(apksdir + '/' + filename):
            temp_smali_info = analysis_certain_apk_smali_by_apkpath(
                apksdir + '/' + filename)
            smali_info.append(temp_smali_info)
    return smali_info


def get_smali_info_by_filename(filename):
    fp = open(filename, 'r')
    smali_matrix = []
    for line in fp.readlines():
        lineArr = line.strip().split(' ')
        temp = []
        for value in lineArr:
            # print value
            if value.find('apkname') >= 0:
                continue
            temp.append(float(value.split(':')[1]))
        smali_matrix.append(temp)
    fp.close()
    return mat(smali_matrix)


def get_smali_info_by_cluster_and_filename(cluster, filename):
    fp = open(filename, 'r')
    smali_matrix = []
    container = []
    for line in fp.readlines():
        lineArr = line.strip().split(' ')
        apkname = lineArr[1].split(':')[1]
        if cluster.count(apkname) == 1:
            container.append(line)
    fp.close()
    for line in container:
        lineArr = line.strip().split(' ')
        temp = []
        for value in lineArr:
            if value.find('apkname') >= 0:
                continue
            temp.append(float(value.split(':')[1]))
        smali_matrix.append(temp)
    return mat(smali_matrix)

def get_test_malicious_smali_info():
    fp=open('cluster_info.txt')
    apknames=[]
    smali_matrix = []
    for line in fp.readlines():
        apknames.append(line.strip().split(' ')[0])
    fp.close()
    fp=open('malicious_smali_info.txt')
    for line in fp.readlines():
        # print line;
        lineArr = line.strip().split(' ')
        apkname=line.strip().split(' ')[1].split(':')[1]
        if apknames.count(apkname)==1:
            continue;
        temp = []
        for value in lineArr:
            if value.find('apkname') >= 0:
                continue
            temp.append(float(value.split(':')[1]))
        smali_matrix.append(temp)
    fp.close()
    return mat(smali_matrix)

    


