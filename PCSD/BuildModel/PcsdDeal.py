# !/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import time
import pickle
sys.path.append('../BuildMatrix/')
sys.path.append('../../Common/')
sys.path.append('../ExtractFeature/')

from Common import *
from BuildMatrix import *
from UnzipApks import *
from Decompile import *
from ClusterDeal import *
from AlgorithmDeal import *
from ClusterDeal import *
from numpy import *


# deal single dex apk
def deal_single_dex_apk(apk_file):
	manifest_info= get_mainfest_info(apk_file)
	dex_info=get_dex_info(apk_file);
	dex_matirx=normalize(hstack((mat(dex_info),mat(manifest_info))))
	rows,cols=shape(dex_matirx)
	result = zeros((rows, cols + 1))
	result[:,0:-1]=dex_matirx
	result[:,-1]=1
	middle_result=deal_matrix_by_cluster(result,"dex");
	fp=open("../Material/dex_clf.pkl");
	clf_info=pickle.load(fp);
	clf=clf_info["clf"]
	print "***********************************************************"
	print "***********************************************************"
	print u"模型2参数",clf_info
	print "***********************************************************"
	print "***********************************************************"
	if clf.predict(middle_result)<clf_info["threshold"]:
		print "benign"
	else:
		print "malicious"

# deal single smali apk
def deal_single_smali_apk(apk_file):
	manifest_info= get_mainfest_info(apk_file)
	smali_info=get_smali_info(apk_file)
	smali_matrix= normalize(hstack((mat(smali_info),mat(manifest_info))))
	rows,cols=shape(smali_matrix)
	result = zeros((rows, cols + 1))
	result[:,0:-1]=smali_matrix
	result[:,-1]=1
	middle_result=deal_matrix_by_cluster(result,"smali");
	fp=open("../Material/smali_clf.pkl");
	clf_info=pickle.load(fp);
	clf=clf_info["clf"]
	print "***********************************************************"
	print "***********************************************************"
	print u"模型1参数",clf_info
	print "***********************************************************"
	print "***********************************************************"
	if clf.predict(middle_result)<clf_info["threshold"]:
		print "benign"
	else:
		print "malicious"

# get malicious family size
def get_malicious_family_size():
	fp=open("../Material/apk_family.csv")
	result={}
	for line in fp.readlines():
		family=line.strip().split(",")[-1];
		if result.get(family)==None:
			result[family]=1
		else:
			result[family]=result[family]+1
	temp=[]
	for key in result:
		a={}
		a[result[key]]=key
		temp.append(a);
	temp.sort(reverse=True);
	print temp[0:21]



# -------------------------------------------------------
# step 1, extract dex feature
# *******************************************************
# unzip_malicious_apk() # get malicious apk dex info
# unzip_benign_apk() # get benign dex info


# -------------------------------------------------------
# step 2, extract smali and manifest feature
# *******************************************************
# decompile_malicious_apk() # get malicious smali and manifest info
# decompile_benign_apk() # get benign smali and manifest info


# -------------------------------------------------------
# step 3, draw cluster graph and select best cluster num
# *******************************************************
# draw_dex_info_cluster() # draw dex info cluster
# draw_smali_info_cluster() # draw smlai info cluster
# draw_cluster() # draw one graph about dex and smali info

# -------------------------------------------------------
# step 3, draw cluster graph and select best cluster num
# *******************************************************
# draw_dex_cluster_inflexion() # draw dex info cluster
# draw_smali_cluster_inflexion() # draw smlai info cluster
# draw_cluster() # draw one graph about dex and smali info



# -------------------------------------------------------
# step 4, save cluster info on the basis of cluster number
# *******************************************************
# save_dex_info_cluster() # save dex info cluster
# save_smali_info_cluster() # save smali info cluster


# -------------------------------------------------------
# step 5, first train and test
# *******************************************************
# get_dex_best_algorithm_per_cluster() # get best algorithm for dex info ,the result will be saved in "../Material/dex_cluster_best_algorithm.pkl"
# get_smali_best_algorithm_per_cluster() # get best algorithm for smali info ,the result will be saved in "../Material/smali_cluster_best_algorithm.pkl"


# -------------------------------------------------------
# step 6, second train and test
# *******************************************************
# train_set=build_dex_train_matrix_2() # for dex
# test_set=build_dex_test_matrix_2() # for dex
# get_optimized_algorithm_by_train_set_and_test_set(train_set,test_set,"dex")
#
#
# train_set=build_smali_train_matrix_2() # for smali
# test_set=build_smali_test_matrix_2() # for smali
# get_optimized_algorithm_by_train_set_and_test_set(train_set,test_set,"smali")


# -------------------------------------------------------
# step 7, deal single apk
# *******************************************************
start=time.time()
apk_file="/Volumes/WD/Experiment/DataSet/BenignApks/com.ua.record.apk"
deal_single_dex_apk(apk_file)
end=time.time();
print u"模型2所需时间 "+str(end-start)

start=time.time()
apk_file="/Volumes/WD/Experiment/DataSet/BenignApks/com.ua.record.apk"
deal_single_smali_apk(apk_file)
end=time.time();
print u"模型1所需时间"+str(end-start)


# -------------------------------------------------------
# step 8, deal comparsion apk
# *******************************************************
# apk_files = []
# for root, dirs, files in os.walk(comparsion_dir):
# 	for file in files:
# 		apk_file = root + '/' + file
# 		if apk_file.find('.apk') >= 0 and apk_file.split("/")[-1][0]!='.':
# 			print apk_file
# 			apk_files.append(apk_file)
# result=[];
# for apk_file in apk_files:
# 	print apk_file
# 	temp={}
# 	temp["apk_file"]=apk_file
# 	start=time.time()
# 	deal_single_dex_apk(apk_file);
# 	end=time.time()
# 	temp["dex_time"]=end-start;
# 	start=time.time()
# 	deal_single_smali_apk(apk_file);
# 	end=time.time();
# 	temp["smali_time"]=end-start;
# 	result.append(temp);
# fp=open("../Material/dex_smali_deal_time.pkl","wb");
# pickle.dump(result,fp)


# -------------------------------------------------------
# train model without clustering
# *******************************************************
# deal with dex info
# entity_dex_layer_1()
# entity_dex_layer_2()



# deal with smali info
# entity_smali_layer_1()
# entity_smali_layer_2()


# -------------------------------------------------------
# step append, deal comparsion apk
# *******************************************************

# get_malicious_family_size()
