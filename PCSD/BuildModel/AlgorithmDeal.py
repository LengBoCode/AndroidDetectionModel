#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
sys.path.append('../../Common/')
sys.path.append('../BuildMatrix/')
from Common import *
from BuildMatrix import *
from sklearn import svm
from sklearn.neural_network import MLPRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neighbors import RadiusNeighborsRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import Ridge
from sklearn import linear_model
from sklearn import tree
from sklearn.metrics import roc_curve, auc

import matplotlib.pyplot as plt
import pickle


# we have seven algorithm as below from algorithm 1 to algorithm 7
def algorithm1(X, y):
    clf = svm.SVR()
    clf.fit(X, y)
    return clf


def algorithm2(X, y):
    clf = MLPRegressor()
    clf.fit(X, y)
    return clf


def algorithm3(X, y):
    clf = RandomForestRegressor()
    clf.fit(X, y)
    return clf


def algorithm4(X, y):
    clf = KNeighborsRegressor()
    clf.fit(X, y)
    return clf


def algorithm5(X, y):
    clf = linear_model.LinearRegression()
    clf.fit(X, y)
    return clf




def algorithm(X, y, n):
    if n == 1:
        return algorithm1(X, y)
    elif n == 2:
        return algorithm2(X, y)
    elif n == 3:
        return algorithm3(X, y)
    elif n == 4:
        return algorithm4(X, y)
    elif n == 5:
        return algorithm5(X, y)
    else:
        print 'algorithm not exist'


# get algorithm name by number
def get_algorithm_name_by_number(n):
    if n == 1:
        return 'SVR'
    elif n == 2:
        return 'MLPRegressor'
    elif n == 3:
        return 'RandomForestRegressor'
    elif n == 4:
        return 'KNeighborsRegressor'
    elif n == 5:
        return 'LinearRegression'
    else:
        print 'algorithm not exist'


# get optimized algorithm for train set and test set
def get_optimized_algorithm_by_train_set_and_test_set(train_set, test_set, kind):
    plt.figure(dpi=80, facecolor=(1, 1, 1))
    best = 0
    final_predict = 0
    final_thresholds = 0
    final_clf = 0
    result_save=[]
    temp_auc=[]
    for x in xrange(1, ALGORITHM_NUM + 1):
        clf = algorithm(train_set[:, 0:-1], train_set[:, -1], x)
        predict = clf.predict(test_set[:, 0:-1]).tolist()
        fpr, tpr, thresholds = roc_curve(
            array(test_set[:, -1]), array(predict), pos_label=1.0)
        get_accuracy(test_set[:, -1], predict, thresholds)
        roc_auc = auc(fpr, tpr)
        temp_auc.append(roc_auc)
        if roc_auc > best:
            best = roc_auc
            final_predict = predict
            final_clf = clf
            final_thresholds = thresholds
        line_label = get_algorithm_name_by_number(x) + u'算法，AUC= ' + str(roc_auc)
        plt.plot(fpr, tpr, lw=1, label=line_label)
    result_save.append(range(1,len(temp_auc)+1))
    result_save.append(temp_auc)
    save_matrix_to_file(mat(result_save).T,"../Material/"+kind+"_final_algorithm.csv")
    print '*************************************************'
    temp = evaluate_result(test_set[:, -1], final_predict)
    print best, temp
    print '*************************************************'
    temp = get_accuracy(
        test_set[:, -1], final_predict, final_thresholds)
    temp["clf"] = final_clf
    temp["auc"] = best
    if kind == "dex":
        fp = open("../Material/dex_clf.pkl", "wb")
    else:
        fp = open("../Material/smali_clf.pkl", "wb")
    pickle.dump(temp, fp)
    plt.ylabel(u'真阳性率(TPR)', fontsize=25)
    xlabel = u'假阳性率(FPR)\n'
    plt.xlabel(xlabel, fontsize=25)
    X_label = linspace(0, 1, 1000)
    Y_label = linspace(0, 1, 1000)
    plt.legend(loc=0, numpoints=1)
    leg = plt.gca().get_legend()
    ltext = leg.get_texts()
    # plt.setp(ltext, fontsize='4')
    plt.plot(X_label, Y_label,)
    plt.show()


# get best algorithm per cluster
def get_best_algorithm_per_cluster(result, test_set, kind):
    if kind == "dex":
        fp = open("../Material/dex_cluster_best_algorithm.pkl", "wb")
    else:
        fp = open("../Material/smali_cluster_best_algorithm.pkl", "wb")
    output = []
    plt.figure(dpi=80, facecolor=(1, 1, 1))
    result_save=[]
    for key in result:
        print "key",key
        train_set = mat(result[key])
        ax = plt.subplot(221 + key)
        best_auc = 0
        final_predict = 0
        final_thresholds = 0
        final_clf = 0
        temp_auc=[]
        for x in xrange(1, ALGORITHM_NUM + 1):
            clf = algorithm(train_set[:, 0:-1], train_set[:, -1], x)
            predict = clf.predict(test_set[:, 0:-1]).tolist()
            fpr, tpr, thresholds = roc_curve(
                array(test_set[:, -1]), array(predict), pos_label=1.0)
            roc_auc = auc(fpr, tpr)
            print roc_auc
            temp_auc.append(roc_auc)
            if best_auc < roc_auc:
                best_auc = roc_auc
                final_predict = predict
                final_thresholds = thresholds
                final_clf = clf
            line_label =  u'第'+str(x) + u'算法，AUC= ' + str(roc_auc)
            plt.plot(fpr, tpr, lw=0.5, label=line_label)
        result_save.append(temp_auc)
        temp= get_accuracy(
            test_set[:, -1], final_predict, final_thresholds)
        # print '*************************************************'
        # temp = evaluate_result(test_set[:, -1], predict)
        # print best, temp
        # print '*************************************************'
        # temp["threshold"] = final_thresholds
        temp["clf"] = final_clf
        temp["auc"] = best_auc
        output.append(temp)
        plt.ylabel(u'真阳性率(TPR)')
        xlabel = u'假阳性率(FPR)\n第' + str(key + 1) + u"簇"
        plt.xlabel(xlabel)
        X_label = linspace(0, 1, 1000)
        Y_label = linspace(0, 1, 1000)
        plt.legend(loc=0, numpoints=1)
        if kind == 'dex' and key == 2:
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width, box.height * 0.8])
            ax.legend(loc='right', bbox_to_anchor=(1.5, 0.8), ncol=1)
        leg = plt.gca().get_legend()
        ltext = leg.get_texts()
        plt.setp(ltext, fontsize='8')
        plt.plot(X_label, Y_label,)
    result_save.append(range(1,len(temp_auc)+1))
    print result_save
    save_matrix_to_file(mat(result_save).T,"../Material/"+kind+"_cluster_algorithm.csv")
    pickle.dump(output, fp)
    plt.show()

# get best algorithm per cluster


def get_entity_best_algorithm(train_set, test_set, kind):
    if kind == "dex":
        fp = open("../Material/entity_dex_clf.pkl", "r")
    else:
        fp = open("../Material/entity_smali_clf.pkl", "r")
    temps = pickle.load(fp)
    clf = temps['clf']
    final_train_set = clf.predict(train_set[:, 0:-1])
    final_test_set = clf.predict(test_set[:, 0:-1])
    temp = []
    temp.append(final_train_set)
    temp.append(train_set[:, -1])
    print temp
    train_set = mat(temp).T
    temp = []
    temp.append(final_test_set)
    temp.append(test_set[:, -1])
    test_set = mat(temp).T
    print shape(train_set), shape(test_set)
    plt.figure(dpi=80, facecolor=(1, 1, 1))
    best = 0
    final_predict = 0
    final_thresholds = 0
    final_clf = 0
    for x in xrange(1, ALGORITHM_NUM + 1):
        clf = algorithm(train_set[:, 0:-1], train_set[:, -1], x)
        predict = clf.predict(test_set[:, 0:-1]).tolist()
        fpr, tpr, thresholds = roc_curve(
            array(test_set[:, -1]), array(predict), pos_label=1.0)
        get_accuracy(test_set[:, -1], predict, thresholds)
        roc_auc = auc(fpr, tpr)
        if roc_auc > best:
            best = roc_auc
            final_predict = predict
            final_clf = clf
            final_thresholds = thresholds
        line_label = 'algo= ' + \
            get_algorithm_name_by_number(x) + ' area= ' + str(roc_auc)
        plt.plot(fpr, tpr, lw=1, label=line_label)
    print '*************************************************'
    temp = evaluate_result(test_set[:, -1], final_predict)
    print best, temp
    print '*************************************************'
    temp= get_accuracy(
        test_set[:, -1], final_predict, final_thresholds)
    temp["clf"] = final_clf
    temp["auc"] = best
    if kind == "dex":
        fp = open("../Material/final_entity_dex_clf.pkl", "wb")
    else:
        fp = open("../Material/final_entity_smali_clf.pkl", "wb")
    print temp
    pickle.dump(temp, fp)
    plt.ylabel('True positive rate', fontsize=25)
    xlabel = 'False positive rate\n'
    plt.xlabel(xlabel, fontsize=25)
    X_label = linspace(0, 1, 1000)
    Y_label = linspace(0, 1, 1000)
    plt.legend(loc=0, numpoints=1)
    leg = plt.gca().get_legend()
    ltext = leg.get_texts()
    # plt.setp(ltext, fontsize='4')
    plt.plot(X_label, Y_label,)
    plt.show()
    # print hstack((final_train_set,train_set[:,-1]))


# get optimized algorithm for train set and test set
def get_entity_optimized_algorithm(train_set, test_set, kind):
    print shape(train_set), shape(test_set)
    plt.figure(dpi=80, facecolor=(1, 1, 1))
    best = 0
    final_predict = 0
    final_thresholds = 0
    final_clf = 0
    for x in xrange(1, ALGORITHM_NUM + 1):
        clf = algorithm(train_set[:, 0:-1], train_set[:, -1], x)
        predict = clf.predict(test_set[:, 0:-1]).tolist()
        fpr, tpr, thresholds = roc_curve(
            array(test_set[:, -1]), array(predict), pos_label=1.0)
        get_accuracy(test_set[:, -1], predict, thresholds)
        roc_auc = auc(fpr, tpr)
        if roc_auc > best:
            best = roc_auc
            final_predict = predict
            final_clf = clf
            final_thresholds = thresholds
        line_label = 'algo= ' + \
            get_algorithm_name_by_number(x) + ' area= ' + str(roc_auc)
        plt.plot(fpr, tpr, lw=1, label=line_label)
    print '*************************************************'
    temp = evaluate_result(test_set[:, -1], final_predict)
    print best, temp
    print '*************************************************'
    temp= get_accuracy(
        test_set[:, -1], final_predict, final_thresholds)
    temp["clf"] = final_clf
    temp["auc"] = best
    if kind == "dex":
        fp = open("../Material/entity_dex_clf.pkl", "wb")
    else:
        fp = open("../Material/entity_smali_clf.pkl", "wb")
    pickle.dump(temp, fp)
    plt.ylabel('True positive rate', fontsize=25)
    xlabel = 'False positive rate\n'
    plt.xlabel(xlabel, fontsize=25)
    X_label = linspace(0, 1, 1000)
    Y_label = linspace(0, 1, 1000)
    plt.legend(loc=0, numpoints=1)
    leg = plt.gca().get_legend()
    ltext = leg.get_texts()
    # plt.setp(ltext, fontsize='4')
    plt.plot(X_label, Y_label,)
    plt.show()

# get best algorithm for dex info


def get_dex_best_algorithm_per_cluster():
    train_set = build_dex_train_matrix_by_cluster_info()
    test_set = build_dex_test_matrix_1()
    get_best_algorithm_per_cluster(train_set, test_set, "dex")


# get best algorithm for smali info
def get_smali_best_algorithm_per_cluster():
    train_set = build_smali_train_matrix_by_cluster_info()
    test_set = build_smali_test_matrix_1()
    get_best_algorithm_per_cluster(train_set, test_set, "smali")


# get best algorithm for entity dex info
def entity_dex_layer_1():
    train_set = build_entity_dex_train_matrix_1()
    test_set = build_entity_dex_test_matrix_1()
    get_entity_optimized_algorithm(train_set, test_set, "dex")

# get best algorithm for entity dex info


def entity_dex_layer_2():
    train_set = build_entity_dex_train_matrix_2()
    test_set = build_entity_dex_test_matrix_2()
    get_entity_best_algorithm(train_set, test_set, "dex")

# get best algorithm for entity smali info


def entity_smali_layer_1():
    train_set = build_entity_smali_train_matrix_1()
    test_set = build_entity_smali_test_matrix_1()
    get_entity_optimized_algorithm(train_set, test_set, "smali")


# get best algorithm for entity smali info
def entity_smali_layer_2():
    train_set = build_entity_smali_train_matrix_2()
    test_set = build_entity_smali_test_matrix_2()
    get_entity_best_algorithm(train_set, test_set, "smali")
