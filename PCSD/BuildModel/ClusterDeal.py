#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
sys.path.append('../../Common/')
sys.path.append('../BuildMatrix/')
from InitMatrix import *
from BuildMatrix import *
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.metrics import calinski_harabaz_score
import matplotlib.pyplot as plt
import pickle
import numpy as np


# create clusters by cluster number and matrix
def create_clusters(matrix, n=4):
    kmeans = KMeans(n_clusters=n)
    kmeans.fit(matrix)
    kmeans_labels = kmeans.labels_
    return kmeans_labels


# get silhouette coefficient
def get_silhouette_coefficient_and_sse(matrix,n=4):
    matrix=normalize(matrix)
    kmeans = KMeans(n_clusters=n)
    kmeans.fit(matrix)
    kmeans_labels = kmeans.labels_
    matrixs={}
    for i in range(n):
        matrixs[i]=[]
    for i in range(len(kmeans_labels)):
        for j in range(n):
            if kmeans_labels[i]==j:
                matrixs[j].append(matrix[i,:].tolist())
    sse=0
    for i in range(n):
        sse+=get_sse(matrixs[i])
    return silhouette_score(matrix,kmeans.labels_),sse/n
    # return calinski_harabaz_score(matrix,kmeans.labels_),sse/n

# # get sse
def get_sse(matrix):
    return np.sum(np.square(matrix-np.mean(matrix,axis=0)))

# draw inflexion
def get_mean_diameter_and_radius_of_all_cluster(matrix, n):
    matrix=normalize(matrix)
    kmeans = KMeans(n_clusters=n)
    kmeans.fit(matrix)
    kmeans_labels = kmeans.labels_
    matrixs={}
    for i in range(n):
        matrixs[i]=[]
    for i in range(len(kmeans_labels)):
        for j in range(n):
            if kmeans_labels[i]==j:
                matrixs[j].append(matrix[i,:].tolist())
    diameter=0
    radius=0
    for i in range(n):
        print "get "+str(i+1)+"th cluster diameter and radius"
        temp_diameter,temp_radius= get_diameter_and_radius_of_cluster(mat(matrixs[i]))
        diameter+=temp_diameter
        radius=temp_radius
    return diameter/n,radius/n

# get diameter and radius of cluster
def get_diameter_and_radius_of_cluster(cluster_matrix):
    center= np.mean(cluster_matrix,axis=0)
    radius=0
    for i in range(shape(cluster_matrix)[0]):
        temp= get_distance_of_two_points(center,cluster_matrix[i,:])
        if temp>radius:
            radius=temp
    diameter=0
    for i in range(shape(cluster_matrix)[0]):
        for j in xrange(i,shape(cluster_matrix)[0]):
                temp= get_distance_of_two_points(cluster_matrix[i,:],cluster_matrix[j,:])
                if temp>diameter:
                    diameter=temp
    print diameter,radius
    return diameter,radius

# how many cluster number is resonable by graph


def draw_cluster_graph(matrix):
    n = 9  # cluster number
    plt.figure(1)
    for x in xrange(3, n):
        repeat = 8
        X = linspace(1, x, x)
        plt.subplot(231 + x - 3)
        for l in xrange(1, repeat):
            labels = create_clusters(matrix, x)
            result = {}
            for j in xrange(0, x):
                result.setdefault(j, 0)
            for i in xrange(0, MALICIOUS_SPLIT_1):
                result[labels[i]] = result[labels[i]] + 1
            Y = []
            for key in result:
                Y.append(result[key])
            Y.sort()
            print Y
            plt.plot(X, Y, label=str(l) + 'th')
        plt.ylabel(u'簇大小')
        plt.xlabel(u'第' + str(x) + u'簇')
        plt.legend(loc=0, numpoints=1)
        leg = plt.gca().get_legend()
        ltext = leg.get_texts()
        plt.setp(ltext, fontsize='4')
    plt.show()




# draw smali info cluster
def draw_smali_info_cluster():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[0:MALICIOUS_SPLIT_1]
    print shape(malicious_matrix)
    draw_cluster_graph(malicious_matrix)


# cluster by smali info
def save_smali_info_cluster():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[0:MALICIOUS_SPLIT_1]
    print shape(malicious_matrix)
    cluster_info = create_clusters(malicious_matrix,6)
    file = "../Material/smali_cluster_info.pkl"
    output = open(file, 'wb')
    pickle.dump(cluster_info, output)

# draw dex info cluster


def draw_dex_info_cluster():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    malicious_matrix = malicious_matrix[0:MALICIOUS_SPLIT_1]
    print shape(malicious_matrix)
    draw_cluster_graph(malicious_matrix)

# cluster by dex info


def save_dex_info_cluster():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    malicious_matrix = malicious_matrix[0:MALICIOUS_SPLIT_1]
    print shape(malicious_matrix)
    cluster_info = create_clusters(malicious_matrix,3)
    file = "../Material/dex_cluster_info.pkl"
    output = open(file, 'wb')
    pickle.dump(cluster_info, output)

# draw one graph about dex and smali


def draw_cluster():
    smali_matrix = get_malicious_manifest_and_smali_matrix()
    smali_matrix = smali_matrix[0:MALICIOUS_SPLIT_1]
    # smali_matrix=normalize(smali_matrix)
    print shape(smali_matrix)
    dex_matrix = get_malicious_manifest_and_dex_matrix()
    dex_matrix = dex_matrix[0:MALICIOUS_SPLIT_1]
    # dex_matrix=normalize(dex_matrix)
    print shape(dex_matrix)
    n = 6  # cluster number
    plt.figure(dpi=80, facecolor=(1, 1, 1))
    repeat = 10

    for x in xrange(3, n):
        X = linspace(1, x, x)
        plt.subplot(231 + x - 3)
        for l in xrange(1, repeat):
            labels = create_clusters(dex_matrix, x)
            result = {}
            for j in xrange(0, x):
                result.setdefault(j, 0)
            for i in xrange(0, MALICIOUS_SPLIT_1):
                result[labels[i]] = result[labels[i]] + 1
            Y = []
            for key in result:
                Y.append(result[key])
            Y.sort()
            print Y
            plt.plot(X, Y, label=str(l) + 'th')
        plt.ylabel('size per cluster')
        plt.xlabel(str(x) + ' clusters of Model 1 ')
        plt.legend(loc=0, numpoints=1)
        leg = plt.gca().get_legend()
        ltext = leg.get_texts()
        plt.setp(ltext, fontsize='4')

    for x in xrange(3, n):
        X = linspace(1, x, x)
        plt.subplot(231 + x)
        for l in xrange(1, repeat):
            labels = create_clusters(smali_matrix, x)
            result = {}
            for j in xrange(0, x):
                result.setdefault(j, 0)
            for i in xrange(0, MALICIOUS_SPLIT_1):
                result[labels[i]] = result[labels[i]] + 1
            Y = []
            for key in result:
                Y.append(result[key])
            Y.sort()
            print Y
            plt.plot(X, Y, label=str(l) + 'th')
        plt.ylabel('size per cluster')
        plt.xlabel(str(x) + ' clusters of Model 2')
        plt.legend(loc=0, numpoints=1)
        leg = plt.gca().get_legend()
        ltext = leg.get_texts()
        plt.setp(ltext, fontsize='4')

    plt.show()


def draw_cluster_inflexion(matrix,file):
    start_cluster_num=2
    end_cluster_num=9
    trial_num=50
    diameter=[]
    radius=[]
    silhouette_coefficient=[]
    sse=[]
    for i in xrange(start_cluster_num,end_cluster_num):
        mean_silhouetter_coefficient=0
        mean_sse=0
        for j in range(trial_num):
            print "running "+str(i)+'th cluster '+str(j+1)+'th trial'
            temp_silhouette_coefficient,temp_sse=get_silhouette_coefficient_and_sse(matrix,i)
            mean_silhouetter_coefficient+=temp_silhouette_coefficient
            mean_sse+=temp_sse
        silhouette_coefficient.append(mean_silhouetter_coefficient/trial_num)
        sse.append(mean_sse/trial_num)
    print silhouette_coefficient
    print sse
    result=[]
    result.append(xrange(2,2+len(silhouette_coefficient)))
    result.append(silhouette_coefficient)
    result.append(sse)
    save_matrix_to_file(mat(result).T,file)

    plt.figure(dpi=80, facecolor=(1, 1, 1))
    plt.plot(range(len(silhouette_coefficient)),silhouette_coefficient)
    plt.plot(range(len(sse)),sse)
    plt.show()


def draw_dex_cluster_inflexion():
    malicious_matrix = get_malicious_manifest_and_dex_matrix()
    malicious_matrix = malicious_matrix[0:MALICIOUS_SPLIT_1]
    print shape(malicious_matrix)
    draw_cluster_inflexion(malicious_matrix,'../Material/dex_cluster_result.csv')

def draw_smali_cluster_inflexion():
    malicious_matrix = get_malicious_manifest_and_smali_matrix()
    malicious_matrix = malicious_matrix[0:MALICIOUS_SPLIT_1]
    print shape(malicious_matrix)
    draw_cluster_inflexion(malicious_matrix,'../Material/smali_cluster_result.csv')
